"""
/// @file     player.py
/// @author   Yoshiki Miyake
/// @date     2020.06.03
/// $version  $
/// $revision $
///
/// (c)2020 Yoshiki Miyake
/// 
"""

import random
import card

#一回目
def player1():
    sum1 = 0
    for i in range(2):
        prand1 = random.randint(0,12)
        prand2 = random.randint(0,3)
        pmark1 = card.mark[prand2]
        pno1 = card.no[prand1]
        pno2 = card.NoString(pno1)
        print("{} の {}".format(pmark1,pno2))
        pno3 = card.one_or_eleven(pno1)
        sum1 += pno3
    print("現在のプレイヤー合計点は、{} 点です。".format(sum1))
    return sum1

#二回目以降
def player2():
    sum1 = 0
    prand1 = random.randint(0,12)
    prand2 = random.randint(0,3)
    pmark1 = card.mark[prand2]
    pno1 = card.no[prand1]
    pno2 = card.NoString(pno1)
    print("プレイヤーは {} の {} が出ました".format(pmark1,pno2))
    pno3 = card.one_or_eleven(pno1)
    sum1 += pno3
    return sum1

#player1()
#player2()
