"""
/// @file     dealer.py
/// @author   Yoshiki Miyake
/// @date     2020.06.03
/// $version  $
/// $revision $
///
/// (c)2020 Yoshiki Miyake
/// 
"""

import random
import card

#一回目
def dealer1():
    sum2 = 0
    for j in range(2):
        drand1 = random.randint(0,12)
        drand2 = random.randint(0,3)
        dmark1 = card.mark[drand2]
        dno1 = card.no[drand1]
        dno2 = card.NoString(dno1)
        if j == 0:
            print("{} の {}".format(dmark1,dno2))
        else:
            print("２枚目のカードは秘密ですよ！！")
        dno3 = card.change(dno1)
        sum2 += dno3
    #print("現在のディーラーの合計点は、{} 点です。".format(sum2))
    return sum2

#二回目移行
def dealer2():
    sum2 = 0
    drand1 = random.randint(0,12)
    drand2 = random.randint(0,3)
    dmark1 = card.mark[drand2]
    dno1 = card.no[drand1]
    dno2 = card.NoString(dno1)
    dno3 = card.change(dno1)
    sum2 += dno3
    return sum2

#dealer1()
#dealer2()
