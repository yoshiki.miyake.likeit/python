"""
/// @file     blackjack.py
/// @author   Yoshiki Miyake
/// @date     2020.06.03
/// $version  $
/// $revision $
///
/// (c)2020 Yoshiki Miyake
/// 
"""

import random
import card
import player
import dealer


class Blackjack:
    def __init__(self):
        print("★☆★☆★ ブラックジャックゲーム開始 ☆★☆★☆")
        print()
        print("プレイヤーのカードは・・・")
        sum1 = 0
        sum2 = 0
        sum1 += player.player1()
        print()
        print("ディーラーのカードは・・・")
        sum2 += dealer.dealer1()
        print()

        while True:
            if sum1 > 21 and sum2 > 21:
                print("プレイヤーとディーラーは共にバーストしました！。")
                break
            if sum1 > 21 and sum2 <= 21:
                print("プレイヤーはバーストしました。")
                break
            if sum2 > 21 and sum1 <= 21:
                print("ディーラーはバーストしました。")
                break

            select = input("１枚引きますか？ Y(引く) か N(引かない) を入力してください。")
            print()

            #プレイヤーがカードを引くパターン
            if select == "Y":

                #プレイヤーのターン
                sum1 += player.player2()
                print("現在のプレイヤー合計点は、{} 点です。".format(sum1))
                print()

                #ディーラーのターン
                if sum2 < 17:
                    sum2 += dealer.dealer2()
                    #print("現在のディーラーの合計点は、{} 点です。".format(sum2))
                    print()
                    

                """ 確認用
                else:
                    print("現在のディーラーの合計点は、{} 点です。".format(sum2))
                    print()
                """


            #プレイヤーがカードを引かないパターン
            elif select == "N":

                #ディーラーのターン
                if sum2 < 17:
                    sum2 += dealer.dealer2()
                    #print("現在のディーラーの合計点は、{} 点です。".format(sum2))
                    print()
                    
                """確認用
                else:
                    print("現在のディーラーの合計点は、{} 点です。".format(sum2))
                    print()
                """

                if sum2 > 21:
                    print("ディーラーはバーストしました。")
                break

            else:
                print("YかNを入力してください。")
                continue

        #最終結果
        print()
        print("ーーーーー結果発表ーーーーー")
        print("現在のプレイヤー合計点は、{} 点です。".format(sum1))
        print("現在のディーラーの合計点は、{} 点です。".format(sum2))

        print()
        if sum1 > 21 and sum2 > 21:
            print("両者引き分け")
        elif sum1 == sum2:
            print("両者引き分け")
        elif sum1 > sum2:
            if sum1 > 21:
                print("ディーラーの勝ち")
            else:
                print("プレイヤーの勝ち")
        elif sum2 > sum1:
            if sum2 > 21:
                print("プレイヤーの勝ち")
            else:
                print("ディーラーの勝ち")

        print()
        print("また遊んでね！！")
              
       
#Blackjack()
