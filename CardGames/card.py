"""
/// @file     card.py
/// @author   Yoshiki Miyake
/// @date     2020.06.03
/// $version  $
/// $revision $
///
/// (c)2020 Yoshiki Miyake
/// 
"""

import random

mark = ["スペード","ハート","ダイヤ","クラブ"]
no = [1,2,3,4,5,6,7,8,9,10,11,12,13]

def NoString(no):
    if no == 1:
        return "A"
    elif no == 11:
        return "J"
    elif no == 12:
        return "Q"
    elif no == 13:
        return "K"
    else:
        return str(no)

def one_or_eleven(no):
    if no == 11 or no == 12 or no == 13:
        return 10
    
    elif no == 1:
        i = input("Lを入力すると１点、その他を入力すると１１点です")
        if i == "L":
            return 1
        else:
            return 11

    else:
        return no

def change(no):
    if no == 11 or no == 12 or no == 13:
        return 10
    else:
        return no
