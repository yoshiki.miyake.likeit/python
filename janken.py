"""
ジャンケンをしたいんだー(((改)))

ジャンケン回数は、1回から10回までランダムです。
ジャンケンに勝利すると、1点。
最終的に点数が高い方が勝ち！
同点の場合は引き分け！！
"""
import random

#ユーザークラス(クラス変数を所持)
class User:
    i = {1:"パー",
         2:"グー",
         3:"チョキ"}
    pass

#プレイヤークラス(ユーザークラスを継承,input)
class Player(User):
    def select(self):
        point = ""
        while True: 
            p = input("パーならP、グーならG、チョキならT、を入力してくだい。")
            if p == "P":
                point = self.i[1]
                break
            elif p == "G":
                point = self.i[2]
                break
            elif p == "T":
                point = self.i[3]
                break
            else:
                print("きちんと値を入力してください。")
                continue
        return point

#ディーラークラス(ユーザークラスを継承,random)
class Dealer(User):
    def rand(self):
        p = random.randint(1, 3)
        point = self.i[p]
        return point

#ゲームクラス
class Game:
    sum1 = 0
    sum2 = 0
    def __init__(self,x):
        self.x = x
    def game(self):
        for i in range(self.x):
            p1 = Player()
            p = p1.select()
            print("プレイヤーは、{} を出しました！".format(p))
            d1 = Dealer()
            d = d1.rand()
            print("ディーラーは、{} を出しました！！".format(d))
            print()
            if p == "パー":
                if d == "パー":
                    pass
                elif d == "チョキ":
                    self.sum2 += 1
                else:
                    self.sum1 += 1
            elif p == "グー":
                if d == "グー":
                    pass
                elif d == "パー":
                    self.sum2 += 1
                else:
                    self.sum1 += 1
            else:
                if d == "チョキ":
                    pass
                elif d == "グー":
                    self.sum2 += 1
                else:
                    self.sum1 += 1
    
    def juge(self):
        if self.sum1 > self.sum2:
            print("プレイヤーの勝ち")
        elif self.sum1 < self.sum2:
            print("ディーラーの勝ち")
        else:
            print("両者引き分け")

            
#プレイゲームクラス
class Playgame:
    rand = random.randint(1,10)
    def playgame(self):
        print("ジャンケン大会!!!")
        print()
        game1 = Game(self.rand)
        game1.game()
        print("結果発表〜〜！！")
        print()
        game1.juge()


#実行
pg = Playgame()
pg.playgame()
